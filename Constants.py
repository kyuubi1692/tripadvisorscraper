NUMBER_OF_RESULTS_PER_LOCATION = 50
CSV_COLUMN_NAMES = ['city', 'rank', 'rating', 'reviews', 'name', 'score', 'latitude', 'longitude']
CITY_CSV_FILE_NAME = "city.csv"
URL_FILE_NAME      = "cityURL.csv"
CITY_RANKED_CSV_FILE_NAME = "cityRanked.csv"
INF = 10000000000
RANK_NORMALIZED_MAX_VALUE = 7.0
RATING_NORMALIZED_MAX_VALUE = 3.0/5.0