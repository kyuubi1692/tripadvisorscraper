from Attraction import Attraction
from Constants import NUMBER_OF_RESULTS_PER_LOCATION
from Scraper import *


class AttractionListScraper(Scraper):
    NUMBER_OF_RESULTS = 50
    SEARCH_STRING = '//*[contains(@class, \'listing\')]'
    PATH_TO_REVIEW_COUNT = 'div[3]/div[2]/div[2]/div[1]/span[2]/a/text()'
    PATH_TO_RATING = 'div[3]/div[2]/div[2]/div[1]/span[1]/img'
    PATH_TO_CATEGORY = 'div[3]/div[2]/div[contains(@class, \'information attraction-type\')]/text()'


    def __init__(self, url, numberOfResults=NUMBER_OF_RESULTS_PER_LOCATION):
        super(AttractionListScraper, self).__init__(url)
        self.numberOfResults = numberOfResults
        self.currentPage = 1


    def __getNameFromElement(self, element):
        return element.xpath('div/a/text()')[0].strip()

    def __getURLFromElement(self, element):
        return element.xpath('div/a')[0].attrib['href']

    def __getReviewCountFromElement(self, element):
        try:
            return ''.join(element.xpath(AttractionListScraper.PATH_TO_REVIEW_COUNT)[0].strip().split()[0].split(','))
        except:
            return 0

    def __getRatingFromElement(self, element):
        try:
            return element.xpath(AttractionListScraper.PATH_TO_RATING)[0].attrib['content']
        except:
            return 0.0

    def __getCategoryFromElement(self, element):
        categories = ''.join(element.xpath(AttractionListScraper.PATH_TO_CATEGORY)).strip().split(';')
        ret = []
        for category in categories:
            category = category.strip()
            if len(category) <= 1:
                continue
            ret.append(category)
        return "|".join(ret)

    def __getLatLongImgForAttraction(self, url):
        scraper = AttractionDetailsScraper(url)
        scraper.scrape()
        address = scraper.getResult()
        return {'latitude': address[0], 'longitude': address[1], 'img': address[2]}

    def __parseElementForAttractionObject(self, element):
        name = self.__getNameFromElement(element)
        reviews = self.__getReviewCountFromElement(element)
        rating = self.__getRatingFromElement(element)
        categories = self.__getCategoryFromElement(element)
        url = self.host + self.__getURLFromElement(element)
        locationAndImage = self.__getLatLongImgForAttraction(url)
        print name, url
        return Attraction(name).setRating(rating).setReviewCount(reviews).setCategory(categories).setURL(
            url).setLocationAndImage(locationAndImage)

    def scrape(self):
        self.openURL(self.url)
        while (len(self.result) < self.numberOfResults):
            tree = self.getTreeData()
            data = tree.xpath(self.SEARCH_STRING)
            for ele in data:
                try:
                    self.result.append(self.__parseElementForAttractionObject(ele))
                except Exception, e:
                    print "E1:", e
                if (len(self.result) >= self.numberOfResults):
                    return
            try:
                self.__openNextPageInResults()
            except Exception, e:
                print "E2:", e
                return



    def __getURLToNextPage(self):
        links = self.browser.get_links()
        for link in links:
            if 'class' in link.attrs:
                if 'paging' in link.attrs['class']:
                    if str(self.currentPage + 1) == link.text:
                        return self._getAbsoluteURL(link.attrs['href'])
        print "Current Page :", self.currentPage
        raise "ERROR: No More Next Page Found. Current Page :", self.currentPage

    def __openNextPageInResults(self):
        urlToNextPage = self.__getURLToNextPage()
        print "URL TO NEXT PAGE:", urlToNextPage
        self.currentPage += 1
        self.openURL(urlToNextPage)