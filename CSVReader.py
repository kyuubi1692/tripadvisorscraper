from Reader import Reader

__author__ = 'malayk'


class CSVReader(Reader):
    def __init__(self, fileName):
        super(CSVReader, self).__init__(fileName)

    def getList(self):
        data = self.read()
        rows = [row.split(',') for row in data.split('\n')]
        return rows