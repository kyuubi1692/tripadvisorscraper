from AttractionListScraper import AttractionListScraper
from CityURLScraper import CityURLScraper
from Constants import *
from writeToCSV import *
import os.path
# done = "Bangkok, Seoul, London, Milan,Paris, Rome, Singapore,  Shanghai"
cities = "New York, Amsterdam,Istanbul,Tokyo,Dubai, Vienna, Kuala Lumpur, Taipei, Hong Kong, Riyadh, Barcelona, Los Angeles"
cities = cities.split(',')
cities = [city.strip().lower() for city in cities]
print cities

writer = writeAttractionsToCSV(CITY_CSV_FILE_NAME)

print "-----------------Initializing Write---------------------"

if os.path.isfile(CITY_CSV_FILE_NAME):
    os.remove(CITY_CSV_FILE_NAME)

class CityURLRetriever(object):
    def __init__(self):
        self.urlWriter = writeToCSV(URL_FILE_NAME, ['city', 'url'])
        if os.path.isfile(URL_FILE_NAME):
            fin = open(URL_FILE_NAME)
            data = fin.read()
            data = data.split('\n')
            self.cityURLDict = dict()
            for row in data:
                if(len(row) <= 0):
                    continue
                row = row.split(',')
                print row
                self.cityURLDict[row[0]] = row[1]
            self.getCityUrl = self.fromFile
        else:
            self.getCityUrl = self.fromURL

    def fromFile(self, city):
        if city in self.cityURLDict:
            return self.cityURLDict[city]
        return self.fromURL(city)

    def fromURL(self, city):
        scraper = CityURLScraper(city)
        scraper.scrape()
        urlToCity = scraper.getResult()
        self.urlWriter.write({'city': city, 'url': urlToCity})
        return urlToCity

urlretriever = CityURLRetriever()
for city in cities:
    urlToCity = urlretriever.getCityUrl(city)
    scraper = AttractionListScraper(urlToCity, 70)
    scraper.scrape()
    listOfAttractions = scraper.getResult()
    print city, len(listOfAttractions), listOfAttractions
    writer.write(listOfAttractions, city)
    print '-------------WRITTEN--------------------'

