from Constants import INF

__author__ = 'malayk'


class City(object):
    def __init__(self, cityName):
        self.cityName = cityName
        self.attractions = list()

        self.minReviewCount = INF
        self.maxReviewCount = 0

    def addAttraction(self, attraction):
        if int(attraction.reviews) > int(self.maxReviewCount):
            self.maxReviewCount = attraction.reviews
        elif int(attraction.reviews) < int(self.minReviewCount):
            self.minReviewCount = attraction.reviews

        self.attractions.append(attraction)

    def __getDict(self):
        return {'name': self.cityName, 'attractions':self.attractions}
    def __str__(self):
        return str(self.__getDict())

    def __repr__(self):
        return str(self.__getDict())