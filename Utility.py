from Attraction import Attraction
from City import City
from Constants import RANK_NORMALIZED_MAX_VALUE, RATING_NORMALIZED_MAX_VALUE


def printNodeTree(element, depth=0):
    for x in xrange(depth):
        print "   ",
    print element.tag,
    if 'class' in element.attrib:
        print '{', element.attrib['class'], "}",
    print
    for ele in element.iterchildren():
        printNodeTree(ele, depth + 1)

def getCityDictFromList(data):
    dictOfCities = dict()
    for row in data:
        cityName = row[0]
        if len(cityName) <= 0:
            continue
        if cityName not in dictOfCities:
            dictOfCities[cityName] = City(cityName)
        attraction = Attraction().initWithAttributes(row[1:])
        dictOfCities[cityName].addAttraction(attraction)
    return dictOfCities

def loopThroughAttractionsInCitiesInDict(dictOfCities):
    for city in dictOfCities:
        for attraction in dictOfCities[city].attractions:
            yield dictOfCities[city],attraction

def getScoreFor(rating, reviews, minVal, maxVal):
    score = (float(reviews)-float(minVal))/float(maxVal)
    score *= RANK_NORMALIZED_MAX_VALUE
    score += float(rating)*RATING_NORMALIZED_MAX_VALUE
    return score