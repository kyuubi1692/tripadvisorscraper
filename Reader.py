__author__ = 'malayk'
class Reader(object):
    def __init__(self, fileName):
        self.data = str()
        self.filename= fileName

    def read(self):
        fout = open(self.filename, "r")
        self.data = fout.read()
        fout.close()
        return self.data
