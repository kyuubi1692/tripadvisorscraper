import itertools
import math
from CSVReader import CSVReader

__author__ = 'malayk'


class CSVToMatrix(object):
    def __init__(self):
        self.graph = dict()

    def getMatrixFor(self, filename):
        csvData = CSVReader(filename).getList()
        attractionMapID = dict()
        reverseMap = dict()
        id = 0
        for row in csvData:
            row = row[0].split(';')
            row = [x.strip('\n\r\"\'') for x in row]
            if len(row) <= 1:
                continue

            if row[2] not in attractionMapID:
                attractionMapID[row[2]] = id
                reverseMap[id] = row[2]
                id += 1
            if row[1] not in attractionMapID:
                attractionMapID[row[1]] = id
                reverseMap[id] = row[1]
                id += 1
        matrix = [[0 for x in xrange(id)] for y in xrange(id)]

        for row in csvData:
            row = row[0].split(';')
            row = [x.strip('\n\r\"\'') for x in row]
            if len(row) <= 1:
                continue
            fromID = attractionMapID[row[2]]
            toID = attractionMapID[row[1]]
            distance = float(row[3])
            matrix[fromID][toID] = distance
            matrix[toID][fromID] = distance

        return reverseMap, matrix


def getScoreFromCSV(filename):
    csvData = CSVReader(filename).getList()
    scoreMap = dict()
    for row in csvData:
        row = row[0].split(';')
        row = [x.strip('\n\r\"\'') for x in row]
        if len(row) <= 1:
            continue
        scoreMap[row[0]] = float(row[1])
    return scoreMap


def getMaxMinTime(matrix):
    maxTime = 0
    minTime = 100000000
    for row in xrange(len(matrix)):
        for col in xrange(len(matrix[row])):
            if matrix[row][col] < 1000000:
                maxTime = max(maxTime, matrix[row][col])
                minTime = min(minTime, matrix[row][col])
    return maxTime, minTime


def normalizeMatrix(matrix):
    maxVal, minVal = getMaxMinTime(matrix)
    maxVal = float(maxVal)
    minVal = float(minVal)

    for row in xrange(len(matrix)):
        for col in xrange(len(matrix[row])):
            if matrix[row][col] < 1000000:
                sqval =float(matrix[row][col])
                matrix[row][col] = ((sqval - minVal) / float(maxVal - minVal)) * 10.0
    return matrix


def computeScoreFor(ordList, matrix, scoreMap):
    score = 0.0
    for idx in xrange(len(ordList) - 1):
        score += scoreMap[idx]
        score -= math.sqrt(matrix[ordList[idx]][ordList[idx + 1]])

    score += scoreMap[-1]
    return score


def areEqual(corOrd, perm, reverseMap):
    caga = list()
    for p in perm:
        caga.append(int(reverseMap[p]))
    for x in xrange(len(corOrd)):
        if corOrd[x] != caga[x]:
            return False
    return True


if __name__ == '__main__':
    reverseMap, matrix = CSVToMatrix().getMatrixFor("rome.csv")

    matrix = normalizeMatrix(matrix)

    scoreMap = getScoreFromCSV("scoreRome.csv")
    print scoreMap
    numOfElements = len(matrix)
    for xx in xrange(4,10):
        permutations = itertools.permutations(range(numOfElements), xx)
        maxScore = 0
        maxPermutation = list()
        for permutation in permutations:
            score = computeScoreFor(permutation, matrix, [scoreMap[reverseMap[idx]] for idx in permutation])
            if score > maxScore:
                maxScore = score
                maxPermutation = [reverseMap[x] for x in permutation]

        print "MaxScore :", maxScore
        print "ANS :", maxPermutation