from Scraper import *


class CityURLScraper(Scraper):
    BASE_URL = 'http://www.tripadvisor.in/Search?q='
    URL_PATH = '//*[@id="SEARCH_RESULTS"]/div[1]/div[3]/div[1]/a'

    def __init__(self, cityName):
        super(CityURLScraper, self).__init__(CityURLScraper.BASE_URL + cityName)

    def scrape(self):
        tree = self.getTreeData()
        data = tree.xpath(CityURLScraper.URL_PATH)[0].attrib['href']
        data = data[len('/Tourism'):]
        data = '/Attractions' + data
        self.result = 'http://www.tripadvisor.in' + data
