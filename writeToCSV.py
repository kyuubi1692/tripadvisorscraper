import os
import re
from Constants import *

__author__ = 'malayk'

class writeToCSV(object):
    def __init__(self, filename, columnNames):
        self.fileName   = filename
        self.rowTuple   = columnNames
        # Uncomment this if you want to add the Column Names at the Top
        # self.__create()


    def __create(self):
        rowDict = dict()
        for i in xrange(len(self.rowTuple)):
            rowDict[self.rowTuple[i]] = self.rowTuple[i]

    def _appendRow(self, rowData):
        fout = open(self.fileName, "a")
        outStr = ""
        for col in xrange(len(self.rowTuple)):
            word = rowData[self.rowTuple[col]]
            if type(word) == type(u'abc'):
                word = word.encode('ascii', 'ignore')
            outStr += str(word) +","
        outStr = outStr[:-1]
        fout.write(outStr+"\n")
        fout.close()


    def write(self, data):
        self._appendRow(data)


class writeAttractionsToCSV(writeToCSV):
    def __init__(self, filename):
        super(writeAttractionsToCSV, self).__init__(filename, CSV_COLUMN_NAMES)

    def _saveImage(self, imgData, name):
        try:
            fout = open(name+".jpg", "w")
        except:
            fout = open("Error.log", "a")
            fout.write(name+"\n")
            fout.close()
            name = ' '.join(re.findall('[A-Za-z]*', name))
            fout = open(name+".jpg", "w")
        fout.write(imgData)
        fout.close()

    def write(self, data, inCity):
        if not os.path.exists(inCity):
            os.makedirs(inCity)
        for row in data:
            row = row.getDict()
            row['city'] = inCity
            os.chdir(inCity)
            self._saveImage(row['image'], row['name'])
            os.chdir("..")
            self._appendRow(row)