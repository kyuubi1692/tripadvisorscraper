from lxml import etree
from robobrowser import RoboBrowser
from Utility import printNodeTree
import urllib2


class Scraper(object):
    def __init__(self, url):
        self.PREFIX = '<bound method BeautifulSoup.get_text of '
        self.url = url
        self.browser = RoboBrowser()
        self.openURL(self.url)
        self.HTML_text = None
        self.host = 'http://www.tripadvisor.in'
        self.result = list()

    def scrape(self):
        raise "Method Not Implemented"

    def openURL(self, url):
        self.browser.open(url)
        self.HTML_text = None

    def _getAbsoluteURL(self, relativeURL):
        return self.host + relativeURL

    def getHTMLData(self):
        if self.HTML_text == None:
            self.HTML_text = str(self.browser.parsed.getText)[len(self.PREFIX):-1]
        return self.HTML_text

    def saveHTMLDataToFile(self, filename="default.html"):
        fout = open(filename, "w")
        fout.write(data)
        fout.close()

    def getTreeData(self, HTMLData=None):
        if (HTMLData == None):
            HTMLData = self.getHTMLData()
        return etree.HTML(HTMLData)

    def getResult(self):
        return self.result


class AttractionDetailsScraper(Scraper):


    PATH_TO_MAP_IMAGE_SPAN = '//*[contains(@class, \'mapWxH\')]'
    PATH_TO_MAP_IMAGE      = 'img'
    PATH_TO_SCRIPT         = '/html/body/script[14]/text()'
    PREFIX                 = 'http://maps.google.com/maps/api/staticmap?channel=ta.desktop&center='
    def __init__(self, url):
        super(AttractionDetailsScraper, self).__init__(url)

    def __getLocationDetails(self, tree):
        imgData = tree.xpath(AttractionDetailsScraper.PATH_TO_MAP_IMAGE_SPAN)
        imgID = imgData[0].xpath('img')[0].attrib['id']
        data = self.getHTMLData().split('\n')
        for line in data:
            if imgID in line and "http" in line:
                idx = line.find("http")
                mapURL  = line[idx:-2]
                geoLocation = mapURL[len(AttractionDetailsScraper.PREFIX):].split('&')[0]
                lat,lon = geoLocation.split(',')
                self.result = [float(lat), float(lon)]
                return
        raise "NO GEO LOCATION FOUND"

    def __getImage(self):
        data = self.getHTMLData().split('\n')
        for line in data:
            if "HERO_PHOTO" in line and "data" in line:
                idx = line.find("http")
                imgUrl = line[idx:-2]
                imgData = urllib2.urlopen(imgUrl).read()
                self.result.append(imgData)
                return
        raise "NO IMAGE"




    def scrape(self):
        self.openURL(self.url)
        tree = self.getTreeData()
        self.__getLocationDetails(tree)
        self.__getImage()



