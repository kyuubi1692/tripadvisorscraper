from Constants import *


class Attraction(object):
    def __init__(self, name=""):
        self.name = name
        self.reviews = -1
        self.rating = -1.0
        self.categories = None
        self.tripAdvisorRating = -1
        self.rank = -1
        self.city = None
        self.score = 0.0
        self.url = ""
        self.latitude = 0.0
        self.longitude = 0.0
        self.image = ""

    def setReviewCount(self, count):
        self.reviews = count
        return self

    def setRating(self, rate):
        self.rating = rate
        return self

    def setURL(self, url):
        self.url = url
        return self

    def setCity(self, cityName):
        self.city = cityName
        return self

    def setCategory(self, categories):
        self.categories = categories
        return self

    def setTripAdvisorRating(self, rate):
        self.tripAdvisorRating = rate
        return self

    def setRank(self, rank):
        self.rank = rank
        return self

    def setScore(self, score):
        self.score = score
        return self

    def setLocationAndImage(self, location):
        self.latitude = location['latitude']
        self.longitude = location['longitude']
        self.image = location['img']
        return self

    def initWithAttributes(self, attribs):
        x = 0
        for attr in CSV_COLUMN_NAMES:
            if attr == 'city':
                continue
            print attr, x, attribs
            setattr(self, attr, attribs[x])
            x += 1
        return self


    def getDict(self):
        return {'city': self.city, 'name': self.name, 'rating': self.rating, 'reviews': self.reviews, 'rank': self.rank,
                'score': self.score, 'latitude': self.latitude, 'longitude': self.longitude, 'image': self.image}

    def __str__(self):
        return str(
            {'city': self.city, 'name': self.name, 'rating': self.rating, 'reviews': self.reviews, 'score': self.score})

    def __repr__(self):
        return str(self.getDict())